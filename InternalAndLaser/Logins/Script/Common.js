﻿function common() {
	var browsersToTest = [btChrome]; //btChrome , btFirefox, btIExplorer
	var browserType = null;
 
	this.CountOfBrowsers = browsersToTest.length;
 
	this.BaseLogin = function(indexInBrowsersToTest, username, password) {
    this.browserType = browsersToTest[indexInBrowsersToTest];
				username = username || "ttangredi";
				password = password || "davis123"; 
    
				var baseURL = ProjectSuite.Variables.baseURL;
				var isRunning = false;
				var isIE = this.browserType == btIExplorer;
				if (isIE) {
							isRunning = Sys.WaitBrowser("iexplore", 3000).Exists;
				}
				if (isRunning) {
							Browsers.Item(this.browserType).Navigate(baseURL);
				} else {
							Browsers.Item(this.browserType).Run(baseURL);
				} 
				var browser = Aliases.browser;
				var page = browser.Page("*");
				this.WaitForPageLoad(page, "Submit");
        		page.Wait(6000);
				var textbox = page.NativeWebObject.Find("id", "Username"); // form.textboxUsername;
				var passwordBox = page.NativeWebObject.Find("id", "Password");
				this.SetText(textbox, username);
				this.SetText(passwordBox, password); 
				var buttonSubmit = page.NativeWebObject.Find("id", "Submit");
				buttonSubmit.ClickButton();
				return browser;		
	}

	// Waits for page to load or 3 seconds, whichever comes first.
	this.WaitForPageLoad = function(page, str) {
		var iterationCount = 0;
		// Loop for 3 seconds or until str appears on the page, whichever comes first.
		do 
		{
			iterationCount++;
			aqUtils.Delay(100); // Creates a delay to allow the page to load.
			var obj = page.NativeWebObject.Find("contentText", str);
		}
		while (iterationCount < 30 && !obj.Exists)
		
		// If the str fails to show in the alloted time, display the error.
		if (!obj.Exists) {
			Log.Error("Waited more than 3 seconds for '" + str + "' to appear.");
		}
	}
  
  this.SetText = function (textbox, input) {
    if (this.browserType === btIExplorer){
		  textbox.Keys(input);
    } else {
		  textbox.SetText(input);
    }
  }
  this.ClickMainMenu = function (page) {
		var mainMenuLink = page.NativeWebObject.Find("id", "header-main-menu-link");
		mainMenuLink.Click();
	}   
}

module.exports.common = common;