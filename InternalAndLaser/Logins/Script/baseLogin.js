﻿var commonLib = require("Common");
var common = new commonLib.common();

// This function runs for logins that are supposed to be successful.
function Login_Success(username, password) {
  baseLogin_Test(username, password, true);
} 

// This function runs for logins that should be invalid.
function Login_Fail(username, password) {
  baseLogin_Test(username, password, false);
} 

function baseLogin_Test(username, password, isTestingSuccess) {
  for (var i = 0; i < common.CountOfBrowsers; i++) {
    var browser = common.BaseLogin(i, username, password);
    var page = browser.Page("*");
  
    // Is login successful? If so, it should go to the menu.
    // If not, it should remain at the login page.
    var str = isTestingSuccess
      ? "Please choose the application, below, that you want to use."
      : "You may have entered a wrong user name or password.";
 
    // Waits for page to load and contain str. If not within 3 seconds, fails. 
    common.WaitForPageLoad(page, str);
  
    // Closes the browser window.
    // Commented out for development purposes.
    browser.BrowserWindow.Close();
  }
} 