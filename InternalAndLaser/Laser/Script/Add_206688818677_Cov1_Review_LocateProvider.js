﻿function Add_206688818677_Cov1_Review_LocateProvider()
{
  var browser;
  var page;
  var page2;
  var section;
  var form;
  var textbox;
  var panel;
  var panel2;
  Browsers.Item(btChrome).Navigate("https://localhost/DavisVision.Member/MsrInternal/Home");
  browser = Aliases.browser;
  page = browser.pageLocalhostDavisvisionMemberMs3;
  page.panel.section.nav.link.Click();
  page2 = browser.pageLocalhostDavisvisionMemberMs2;
  page2.Wait();
  page2.panel.section.nav.link2.Click();
  page2 = browser.pageLocalhostDavisvisionMemberMs5;
  page2.Wait();
  section = page2.sectionMemberIdSearchBindingArea;
  form = section.form;
  textbox = form.textboxMemberId;
  textbox.Click(50, 19);
  textbox.SetText("206688818677");
  form.buttonSubmit.ClickButton();
  section.table.cell.buttonSelect.ClickButton();
  page2 = browser.pageLocalhostDavisvisionMemberMs6;
  page2.Wait();
  page2.panel.section.link3.textnode.Click(58, 6);
  page2 = browser.pageLocalhostDavisvisionMemberMs9;
  page2.Wait();
  page2.panel.section.link.textnode.Click(64, 8);
  page2 = browser.pageLocalhostDavisvisionMemberMs8;
  page2.Wait();
  panel = page2.panel;
  form = panel.section.form;
  textbox = form.textboxZipCode;
  textbox.Click(40, 18);
  textbox.SetText("12180");
  panel2 = form.panel;
  panel2.buttonSearchNow.ClickButton();
  panel2.buttonClearForm.ClickButton();
  // Goes back to MSR Main Menu
  panel.link.Click();
  page.Wait();
}