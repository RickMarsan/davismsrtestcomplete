﻿var commonLib = require("Common");
var common = new commonLib.common();

// This function runs for logins that are supposed to be successful.
function GroupSearch_Test(group) {
   base_GNLookup(group);
}
//COMMIT TESTING - DELETE AFTER VERIFICATION
function base_GNLookup(group) {
	for (var i = 0; i < common.CountOfBrowsers; i++) {
		var browser = common.BaseLogin(i);
		var page = browser.Page("*");//page = browser.pageLocalhostDavisvisionMemberMs2;
		// Waits for page to load or 3 seconds, whichever comes first.
		common.WaitForPageLoad(page, "Group Name Search");
		var menuItem = page.NativeWebObject.Find("innerText", "Group Name Search");
		menuItem.Click();
		//  	page2 = browser.pageLocalhostDavisvisionMemberMs;
		page.Wait(3000);
		var textbox = page.NativeWebObject.Find("id", "GroupName");
		common.SetText(textbox, group);
		var buttonSubmit = page.NativeWebObject.Find("id", "SearchButton");
		buttonSubmit.ClickButton();

		if (group == "Fail") {
			var noGroupText = page.NativeWebObject.Find("innerText", "No groups contain that text.");
			if (strictEqual(noGroupText, null)) {
				Log.Error("Unable to find any group names on the page that included the text '" + group + "'.");
			}
		} else {
			// Find all elements that are type TD with attribute “data-bind” value of “html: GroupName”
			var tdElements = page.EvaluateXPath("//td[@data-bind='html: GroupName']");
			if (strictEqual(tdElements, null)) {
				Log.Error("Unable to find any group names on the page that included the text '" + group + "'.");
			}

			// Loop through elements, ensuring text includes “ADVANTAGE”
			for (var i = 0; i < tdElements.length; i++)
				for (i in tdElements) {
					var toCheck = tdElements[i];
					if (toCheck.textContent.toUpperCase().indexOf(group) === -1) {
						Log.Error("Could not find '" + group + "'in the row with text '" + toCheck + "'.");
					}
				}
		}
		common.ClickMainMenu(page);
		return browser;
		//	browser.BrowserWindow.Close();
	}
}