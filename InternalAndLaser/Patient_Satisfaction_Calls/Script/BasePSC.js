﻿var commonLib = require("Common");
var common = new commonLib.common();

// This function runs the patient satisfaction calls unit test in desired browser.
function loggedCall_Test(memberID, callerName, reasonMsg, resolutionMsg) { 
		 basePatientSatisfaction(memberID, callerName, reasonMsg, resolutionMsg);
}

function basePatientSatisfaction(memberID, callerName, reasonMsg, resolutionMsg) {
		 for (var i = 0; i < common.CountOfBroswers; i++) {
		 	 var browser = common.BaseLogin(i);
			 var page = browser.Page("*");
    		 page.Wait(3000);
    		 var menuItem = page.NativeWebObject.Find("innerText", "Patient Satisfaction Calls");
    		 menuItem.Click();
    		 //page = browser.pageLocalhostDavisvisionMemberMs;
    		 page.Wait(3000);
    		 var textbox = page.NativeWebObject.Find("id", "MemberIDPartial"); // form.textboxMemberID;
    		 common.WaitForPageLoad(page, "Patient Satisfaction Calls");
    		 common.SetText(textbox, memberID);
    		 var buttonSubmit = page.NativeWebObject.Find("id", "SearchButton");
    		 buttonSubmit.ClickButton();
    		 var selectCallerName = page.NativeWebObject.Find("id", "SelectedCallerName");
    		 var selectReason = page.NativeWebObject.Find("id", "SelectedReason");
    		 var selectedResolution = page.NativeWebObject.Find("id", "SelectedResolution");
    		 selectCallerName.ClickItem(callerName);
    		 selectReason.ClickItem(reasonMsg);
    		 selectedResolution.ClickItem(resolutionMsg);
    		 var buttonAddCall = page.NativeWebObject.Find("id", "AddCall");
    		 buttonAddCall.ClickButton();
    		 return browser;
			} 			
}

function basePatientSatisfactionAltCoverage(memberID, callerName, reasonMsg, resolutionMsg) {
  for (var i = 0; i < common.CountOfBroswers; i++) {
    var browser = common.BaseLogin(i);
    var page = browser.Page("*");
    page.Wait(3000);
				var menuItem = page.NativeWebObject.Find("innerText", "Patient Satisfaction Calls");
    menuItem.Click();
    page.Wait(3000);
    var textbox = page.NativeWebObject.Find("id", "MemberIDPartial"); // form.textboxMemberID;
		  common.WaitForPageLoad(page, "Patient Satisfaction Calls");
				common.SetText(textbox, memberID);
				var buttonSubmit = page.NativeWebObject.Find("id", "SearchButton");
				buttonSubmit.ClickButton();
				
				var buttonSelectId = 0;
				
				//TODO
				//var buttonExists = bool
			 //var buttonExists = this.browserType == btIExplorer; 
					
				while (buttonExists ){
				    var buttonSelect = page.NativeWebObject.Find("id", "SelectButton-0");
								page.Wait(3000);
				    buttonSelect.ClickButton();
					   page.Wait(3000);	
								var selectCallerName = page.NativeWebObject.Find("id", "SelectedCallerName");
				    var selectReason = page.NativeWebObject.Find("id", "SelectedReason");
				    var selectedResolution = page.NativeWebObject.Find("id", "SelectedResolution");
				    selectCallerName.ClickItem(callerName);
				    selectReason.ClickItem(reasonMsg);
				    selectedResolution.ClickItem(resolutionMsg);
				    var buttonAddCall = page.NativeWebObject.Find("id", "AddCall");
				    buttonAddCall.ClickButton();
								
								// Click the submit button again to test the next coverage plan.
							 buttonSubmit.ClickButton();
								
								buttonSelectId +=1;
								//var buttonExists = ,"SelectButton-" + buttonSelectId)
								// TODO
								/*if (buttonExists) {
											Browsers.Item(this.browserType).Navigate(baseURL);
								} else {
											Browsers.Item(this.browserType).Run(baseURL);
								} */
				}  		
    return browser;
		}
}